	function clearNumbers(){
	 	document.getElementById("num1").value = '';
	 	document.getElementById("num2").value = '';
	 	document.getElementById("result").innerHTML = "Result";
	 	document.getElementById("operation").innerHTML = "Operation will appear here.";

	 }

	function addNumbers() {
		let num1 = Number(document.getElementById('num1').value);
		let num2 = Number(document.getElementById('num2').value);

		document.getElementById('operation').innerHTML = "Addition";
		document.getElementById('result').innerHTML = num1 + num2;
		return false;
	}

	function subtractNumbers() {
		let num1 = Number(document.getElementById('num1').value);
		let num2 = Number(document.getElementById('num2').value);

		document.getElementById('operation').innerHTML = "Subtraction";
		document.getElementById('result').innerHTML = num1 - num2;
		return false;
	}

	function multiplyNumbers() {
		let num1 = Number(document.getElementById('num1').value);
		let num2 = Number(document.getElementById('num2').value);

		document.getElementById('operation').innerHTML = "Multiplication";
		document.getElementById('result').innerHTML = num1 * num2;
		return false;
	}

	function divideNumbers() {
		let num1 = Number(document.getElementById('num1').value);
		let num2 = Number(document.getElementById('num2').value);

		document.getElementById('operation').innerHTML = "Division";
		document.getElementById('result').innerHTML = num1 / num2;
		return false;
	}

	function modulusNumbers() {
		let num1 = Number(document.getElementById('num1').value);
		let num2 = Number(document.getElementById('num2').value);

		document.getElementById('operation').innerHTML = "Modulus";
		document.getElementById('result').innerHTML = num1 % num2;
		return false;
	}

	document.getElementById('add').addEventListener('click', addNumbers);
	document.getElementById('minus').addEventListener('click', subtractNumbers);
	document.getElementById('multiply').addEventListener('click', multiplyNumbers);
	document.getElementById('divide').addEventListener('click', divideNumbers);
	document.getElementById('modulus').addEventListener('click', modulusNumbers);